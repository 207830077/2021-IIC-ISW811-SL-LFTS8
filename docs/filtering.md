[<- Volver](/README.md)

# Restricciones avanzadas de consultas elocuentes

Sigamos jugando con **Post** el **filter()** alcance de la consulta de nuestro modelo . Tal vez podamos filtrar adicionalmente las publicaciones según su categoría. Si adoptamos este enfoque, tendremos una forma poderosa de combinar filtros. Por ejemplo, ***" dame todas las publicaciones, escritas por tal o cual autor, que estén dentro de la categoría dada e incluyan el siguiente texto de búsqueda " .***

Se debe modificar el archivo **app/Models/Post.php**

```php
public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? false, function ($query, $search){
            $query
                ->where('title', 'like', '%' . $search . '%')
                ->orWhere('body', 'like', '%' . $search . '%');
        });

        $query->when($filters['category'] ?? false, function ($query, $category){
            $query
                ->whereExists(
                    $query->from('categories')
                     ->where('categories.id', 'posts.category_id')
                     ->where('categories.slug', $category)
                );
        });
    }

public function getRouteKeyName(){
    return 'slug';
}

public function category(){
    return $this->belongsTo(Category::class);
}
```

Luego de esto se modifica el archivo **app/Http/Controller/PostController.php**

```php
public function index()
{
    return view('posts', [
        'posts' => Post::latest()->filter(request(['search', 'category']))->get(),
        'categories' => Category::all(),
        'currentCategory' => Category::where('slug', request('category'))->first()
    ]);
}

public function show(Post $post)
{
    return view('post', [
        'post' => $post
    ]);
}
```
Además se modifica la vista la cual esta en **resources/views/_post-header.blade.php**

```php
@foreach ($categories as $category)
<!--{{ isset($currentCategory) && $currentCategory->is($category) ? 'bg-blue-500 text-white' : '' }}-->
    <x-dropdown-items href="/?category={{ $category->slug }}"
    :active="request()->is('categories/' . $category->slug )">
    <!--:active='request()->is("categories/.{$category->slug}")'>-->
        {{ ucwords($category->name) }}
    </x-dropdown-items>
@endforeach
```

# Extraer un componente de hoja desplegable de categoría

¿Ha notado que cada ruta debe pasar una colección de categorías a la postsvista? Si echas un vistazo, solo se hace referencia a esa variable como parte del menú desplegable de la categoría principal. Entonces, con eso en mente, ¿qué pasaría si creáramos un x-category-dropdown componente dedicado que podría ser responsable de obtener cualquier dato que requiera el menú desplegable? Averigüemos cómo permitir eso en este episodio. 

Primero se debe ejecutar el comando 

     php artisan make:component CategoryDopdown

Luego de esto se crean varios archivo que se deben agregar codigo

*   **resources/views/componets/category-dropdown.blade.php**

```php
<div>
    <x-slot name="trigger">
        <button
        class="py-2 pl-3 pr-9 text-sm font-semibold w-full lg:w-32 text-left flex lg:inline-flex"
        >
        {{ isset($currentCategory) ? ucwords($currentCategory->name) : 'Categories' }}
        <x-icon name="down-arrow" class="absolute pointer-events-none" style="right: 12px;">

        </x-icon>

        </button>
    </x-slot>

    <x-dropdown-items href="/" :active="request()->routeIs('home')">
        All
    </x-dropdown-items>

    @foreach ($categories as $category)

        <x-dropdown-items href="/categories/{{ $category->slug }}"
        :active="request()->is('categories/' . $category->slug )">
            {{ ucwords($category->name) }}
        </x-dropdown-items>
    @endforeach
</div>
```

* **app/Http/View/CategoryDropdown.php**

```php
class CategoryDopdown extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.category-dopdown');
    }
}
```
En el archivo **resources/views/_post-header.blade.php**

```php
<header class="max-w-xl mx-auto mt-20 text-center">
    <h1 class="text-4xl">
        Latest <span class="text-blue-500">Laravel From Scratch</span> News
    </h1>

    <div class="space-y-2 lg:space-y-0 lg:space-x-4 mt-4">
        <!--  Category -->
        <div class="relative lg:inline-flex bg-gray-100 rounded-xl">

        <x-catetegory-dropdown></x-catetegory-dropdown>
            

        </div>
        <div class="relative flex lg:inline-flex items-center bg-gray-100 rounded-xl px-3 py-2">
            <form method="GET" action="#">
                <input type="text" name="search" placeholder="Find something"
                        class="bg-transparent placeholder-black font-semibold text-sm"
                        value="{{ request('search') }}">
            </form>
        </div>
    </div>
</header>
```
# Filtrado de autor

A continuación, agreguemos soporte para filtrar publicaciones de acuerdo con su autor respectivo. Con esta última pieza del rompecabezas, ahora podremos ordenar fácilmente todas las publicaciones por categoría, autor, texto de búsqueda o todo lo anterior.

Modificamos el archivo **PostController** en la ruta **app/Http/Controllers/PostController.php**

```php
public function index()
{
    return view('posts.index', [
        'posts' => Post::latest()->filter(request(['search', 'category', 'author']))->get(),
        'categories' => Category::all()
    ]);
}

public function show(Post $post)
{
    return view('posts.show-post', [
        'post' => $post
    ]);
}
```

Seguidamente modificamos el archivo **app/Models/Post.php**

```php
public function scopeFilter($query, array $filters)
{
    $query->when($filters['search'] ?? false, function ($query, $search){
        $query
            ->where('title', 'like', '%' . $search . '%')
            ->orWhere('body', 'like', '%' . $search . '%');
    });

    $query->when($filters['category'] ?? false, function ($query, $category){
        $query
            ->whereExists(
                $query->from('categories')
                    ->where('categories.id', 'posts.category_id')
                    ->where('categories.slug', $category)
            );
    });

    $query->when($filters['author'] ?? false, function ($query, $author){
        $query
            ->whereExists(
                $query->from('authors')
                    ->where('authors.username', $author)
            );
    });
}
```

Posteriormete modificamos las diferentes vistas 

* **post-cards.blade.php**

```php
<div class="ml-3">
    <h5 class="font-bold">
        <a href="/authors/{{ $post->author->username }}">{{ $post->author->name }}</a>
    </h5>
</div>
```

* **post-featured-card.blade.php**

```php
<div class="ml-3">
    <h5 class="font-bold">
        <a href="/authors/{{ $post->author->username }}">{{ $post->author->name }}</a>
    </h5>
</div>
```

* **post.blade.php**

```php
<div class="ml-3 text-left">
    <h5 class="font-bold">
        <a href="/authors/{{ $post->author->username }}">{{ $post->author->name }}</a>
    </h5>
</div>
````

Modificamos la ruta **author**

```php
Route::get('authors/{author:username}', function (User $author) {
    return view('posts.index', [
        'posts' => $author->posts,
        'categories' => Category::all()
    ]);
});
```

# Solucionar un error de consulta elocuente confuso

Parece que tenemos un pequeño error dentro del filter() alcance de nuestra consulta. En esta lección, revisaremos la consulta SQL subyacente que está produciendo resultados incorrectos y luego arreglaremos el error en nuestra consulta Eloquent.



Modificamos el archivo **app/Models/Post.php**

```php
$query->when($filters['search'] ?? false, function ($query, $search){
    $query->where(fn($query) =>
            $query->where('title', 'like', '%', $search . '%')
                ->orWhere('body', 'like', '%', $search . '%')
        );
});
```

