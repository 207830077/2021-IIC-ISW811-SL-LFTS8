[<- Volver](/README.md)

# Paginación ridículamente simple

Actualmente, estamos obteniendo todas las publicaciones de la base de datos y las representamos como una cuadrícula en la página de inicio. Pero, ¿qué sucede en el futuro cuando tienes, digamos, quinientas publicaciones en el blog? Eso es demasiado costoso de renderizar. La solución es aprovechar la paginación y, afortunadamente, Laravel hace todo el trabajo por usted. Ven a verlo tu mismo.

Se modifica el archivo **app/Http/Controllers/PostController.php**

```php
public function index()
{
    return view('posts.index', [
        'posts' => Post::latest()->filter(request(['search', 'category', 'author']))->paginate(6),
        'categories' => Category::all()
    ]);
}
```

Luego de esto se modifica el archivo **resources/views/post/index.blade.php**

```php
<x-layout>
    @include('posts._header')

    <main class="max-w-6xl mx-auto mt-6 lg:mt-20 space-y-6">
        @if ($posts->count())
            <x-posts-grid :posts="$posts"/>

            {{ $posts->links() }}

        @else
        <p class="text-center">No post yet.Please check back later.</p>
        @endif
    </main>
</x-layout>
```
En el archivo **resources/views/components/categor-dropdown.blade.php**

```php
<x-dropdown-item href="/?category={{ $category->slug }}&
    {{ http_build(request()->except('category', 'page')) }}"
    :active="request()->routeIs('home')">All</x-dropdown-item>

@foreach ($categories as $category)
    <x-dropdown-items href="/?category={{ $category->slug }}&{{ http_build(request()->except('category', 'page')) }}"
    :active="request()->is('categories/' . $category->slug )">
        {{ ucwords($category->name) }}
    </x-dropdown-items>
@endforeach
```

Luego de esto en la terminal se ejecuta el siguiente comando

    php artisan vendor:publish

Seguidamente seleccionar la opción, para completar la publicación

    17

Con esto crea las diferentes paginaciones que puede utilizar a su gusto, con esto termina la seccion de _Paginación_

