[<- Volver](/README.md)

# Búsqueda (The Messy Way)

En esta nueva sección, implementaremos la función de búsqueda para nuestro blog. Voy a demostrar esto en dos pasos. Primero, en este video, simplemente lo haremos funcionar. El código no será reutilizable ni bonito, ¡pero funcionará! Luego, en el siguiente episodio, podemos refactorizar un poco.

Se modificae el archivo en la ruta **resources/views/_post-header.blade.php**

```php
<div class="relative flex lg:inline-flex items-center bg-gray-100 rounded-xl px-3 py-2">
    <form method="GET" action="#">
        <input type="text" name="search" placeholder="Find something"
                class="bg-transparent placeholder-black font-semibold text-sm"
                value="{{ request('search') }}">
    </form>
</div>
```

Seguidamente se modifica el archivo **routes/web.php**

```php
Route::get('/', function () {
    $posts = Post::latest();
    if(request('search')){
        $posts
            ->where('title', 'like', '%' . request('search') . '%')
            ->orWhere('body', 'like', '%' . request('search') . '%');
    }
    return view('posts', [
        'posts' => $posts->get(),
        'categories' => Category::all()
    ]);
})->name('home');
```

# Buscar (la forma más limpia)

Ahora que nuestro formulario de búsqueda está funcionando, podemos tomarnos unos momentos para refactorizar el código en algo más agradable a la vista (y reutilizable). En este episodio, no solo veremos finalmente las clases de controladores, sino que también aprenderemos sobre los ámbitos de consulta de Eloquent.

Lo primero es crear un **controller** para los posts, esto se hace con el siguiente comando:

    php artisan migrate:controller PostController

Una vez creado el archivo **APP/Http/Controllers/PostController.php** se procede a agregar código para su funcionalidad

```php
public function index()
    {
        return view('posts', [
            'posts' => Post::latest()->filter(request(['search']))->get(),
            'categories' => Category::all()
        ]);
    }

    public function show(Post $post)
    {
        return view('post', [
            'post' => $post
        ]);
    }
```

Luego de esto se procede a modificar el modelo, en el archivo **app/Models/Post.php**

```php
public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? false, function ($query, $search){
            $query
                ->where('title', 'like', '%' . $search . '%')
                ->orWhere('body', 'like', '%' . $search . '%');
        });
    }
```

Seguidamente se modifica el archivo **routes/web.php**

```php
Route::get('/', [PostController::class, 'index'])->name('home');

Route::get('posts/{post:slug}', [PostController::class, 'show']);
```

Con esto termina esta sección. 
