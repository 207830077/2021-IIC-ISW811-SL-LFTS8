[<- Volver](/README.md)

# Como una ruta carga una vista

En la sección de **routes** se encuentra el archivo de rutas, en laravel se llama **web.php** es donde se realiza el enrutamiento donde se realizan las peticiones en ese caso por metodo _get_ en la raíz que es donde procese a cargar la vista principal de laravel. Esto de cargar las vistas es posible porque al crear una **ruta** se hace un **return view(_aqui_la_vista_)**, el enrutamiento es fundamental porque cuando se realiza peteciones se espera enrutar a otros lugares de la aplicación por medio de la URL.

# Incluir CSS y JavaScript

Para poder incluir css y javascript en un proyecto de laravel en la vista, se debe referenciar en dicha vista, pero antes, estos archivos tanto css como javascript se debe ser creados y almacenados en la carpeta **public** pues es ahí donde se debe crear los archivos para luego en la vista ser referenciados. 

De la siguiente manera se incluye CSS, con la etiqueta: 

```html
    <link rel="stylesheet" href="public/css/estilo.css">
```

Un pequeño código CSS

```css
body{
    background: white;
    color: #222222;
    max-width: 600px;
    margin: auto;
    font-family: sans-serif;
}

p{
    line-height: 1.6;
}

article + article{
    margin-top: 3rem;
    padding-top: 3rem;
    border-top: 1px solid #c5c5c5;
}
```

# Hacer una ruta y vincularla

La forma más simple de nuestro blog seguramente consistirá en una lista de extractos de publicaciones de blog, que luego enlazan individualmente a una página diferente que contiene la publicación completa. Comencemos a trabajar hacia ese objetivo.

Se crea la vista **post.blade.php**

```html
<!DOCTYPE html> 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>POST</title>
</head>
<body>
    <h1>Mi primer Post</h1>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>
</body>
</html>
```

Luego se cargar la **ruta**

```php
Route::get('/', function () {
    return view('post');
});
```

# Almacenar publicaciones de blog como archivos HTML

Antes de buscar una base de datos, analicemos cómo almacenar cada publicación de blog dentro de su propio archivo HTML. Luego, en nuestro archivo de rutas, podemos usar un comodín de ruta para determinar qué publicación debe buscarse y pasarse a la vista.

En la carpeta **route**, archivo _web.app_ que es donde se crean las rutas se digita el siguiente codigo

```php
Route::get('post', function () {
    return view('post', [
        'post' => '<h1>Hello World</>' 
    ]);
});
```

En la vista, en el archivo **post.blade.php***

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Blog</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
<article>
    <?= $post; ?>
</article>

<a href="/">Go Back</a>
</body>
</html>
```

# Restricciones de comodines de ruta

A veces, deseará limitar un comodín de ruta a solo una determinada secuencia o patrón de caracteres. Afortunadamente, Laravel hace que esto sea pan comido. En este episodio, agregaremos una restricción a nuestra ruta para asegurarnos de que el slug de la publicación del blog consista exclusivamente en cualquier combinación de letras, números, guiones y guiones bajos.

Para realizar la restricción, se va utilizar el codigo que se ha venido trabajado, con un simple **where** se crea una restricción. El siguiente codigo deja más claro como crearla.

```php
Route::get('post/{post}', function ($slug) {
    $path = __DIR__ . "/../resources/posts/{$slug}.html";

    if(!file_exists($path)){
        //dd('file does not exist');
        //abort(404);
        return redirect('/'); 
    }

    $post = file_get_contents($path);
    return view('post', [
        'post' => $post 
    ]);
})->where('post', '[A-z_\-]+');
```

# Utilice el almacenamiento en caché para operaciones costosas

_file_get_contents()_ No es ideal buscar cada vez que se ve una publicación de blog. Piénselo: si diez mil personas ven una publicación de blog al mismo tiempo, eso significa que está llamando _file_get_contents()_ diez mil veces. Eso seguramente parece un desperdicio, particularmente cuando las publicaciones de blog rara vez cambian. ¿Qué pasa si, en cambio, guardamos en caché el HTML de cada publicación para mejorar el rendimiento? Aprenda cómo en este episodio.

El siguiente codigo es utilizado para el almacenamiento en caché

```php
Route::get('post/{post}', function ($slug) {
    $path = __DIR__ . "/../resources/posts/{$slug}.html";

    if(!file_exists($path)){
        //dd('file does not exist');
        //abort(404);
        return redirect('/'); 
    }

    $post = cache()->remember("post.{$slug}", 5, function () use ($path){
        var_dump('file_get_contents');
        return file_get_contents($path);
    });
    return view('post', [
        'post' => $post 
    ]);
})->where('post', '[A-z_\-]+');
```

# Utilice la clase de sistema de archivos para leer un directorio

Ahora averigüemos cómo buscar y leer todas las publicaciones dentro del **resources/posts** directorio. Una vez que tenemos una matriz adecuada, podemos recorrerlos y mostrarlos en la página principal de descripción general del blog.

Dentro de la carpeta **routes** en el archivo **web.php** que es el que se encarga de las rutas desplegamos el siguiente código:

```php
Route::get('/', function () {
    return view('posts', [
        'post' => Post::all()
    ]);
});
```

Luego de esto dentro de la carpeta **app/Models** creamos una clase en este caso se llamara **Post.php** y contendrá la _function_ **all()** que fue nombrada en el archivo **routes/web.php**, con el siguiente código:

```php
public static function all(){
        return File::files(resource_path("posts/"));
    }
```

Posteriormente en el archivo **resources/views/posts.blade.php** se realiza un pequeño código _php_ para que este muestre todo el **resource path**, con el siguiente código:
```html
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>POST</title>
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <?php
            foreach($post as $p) :
        ?>
        <article>
            <?php echo $p; ?>
        </article>
        <?php endforeach; ?>
    </body>
    </html>
```

# Encuentre un paquete de Composer para publicar metadatos

Al final del episodio anterior, consideramos agregar metadatos en la parte superior de cada archivo de publicación. Resulta que este formato de metadatos tiene un nombre: **Yaml Front Matter.** **Veamos si podemos encontrar un paquete de Composer** que nos ayude a analizarlo. Esto nos dará una buena oportunidad de aprender lo fácil y útil que es Composer.

Primeramente se debe instalar **Yaml Front Matter** via composer, esto se hace de la siguiente manera, desde la terminal con el comando:

    composer require spatie/yaml-front-matter

Una vez instalado se procede a codificar, para el uso del metadato ya instaldo procedemos a digitar el siguiente código:

En el archivo **routes/web.php** 

```php
Route::get('/', function () {
    $document = YamlFrontMatter::parseFile(resource_path('posts/my-fourth-post.html'));
    ddd($document->title);
});
```

En el archivo **resources/posts/my-fourth-post.html**

```html
---
title: Mi Cuarto Post
excerpt:Lorem ipsum dolor sit, amet consectetur adipisicing elit. 
date: 2021-06-01
---

<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugiat labore, ad sunt quae quas illum doloremque vitae facere,
suscipit ducimus facilis in officia, animi commodi voluptatibus praesentium nostrum debitis perspiciatis.</p>
```

Una vez hecho lo anterior **Yaml Front Matter** se encarga de hacer lo que le solicitamos, en este caso mostrar un **title, date y hara un excerpt**

Otro ejercicio utilizando **Yaml Front Matter**, en el archivo **routes/web.php** 

```php
Route::get('/', function () {
    $files = File::files(resource_path("posts/"));  
    $posts = [];
    foreach ($files as $file) {
        $document = YamlFrontMatter::parseFile($file);
        $posts [] = new Post(
            $document->title,
            $document->excerpt,
            $document->date,
            $document->body 
        );
    }

    ddd($posts);
});
```

En la clase **Posts** dentro de **app/Models/Post.php**

```php
public $title;
    public $excerpt;
    public $date;
    public $body;

    public function __construct($title, $excerpt, $date, $body){
        $this->title = $title;
        $this->excerpt = $excerpt;
        $this->date = $date;
        $this->body = $body;
    }

    public static function all(){
        $files = File::files(resource_path("posts/"));  
        return array_map(function ($file) {
            return $file->getContents(); 
        }, $files);
    }
```

Esto ejecutara todas la peticiones que se realizaron con **Yaml Front Matter** ya sea agregar o quitar datos, en este caso creamos  variables globales e hizimos un contructor correspondiente.

# Actualización de clasificación y almacenamiento en caché de colecciones

Cada publicación ahora incluye la fecha de publicación como parte de sus metadatos; sin embargo, el feed no está actualmente ordenado según esa fecha. Afortunadamente, debido a que estamos usando colecciones de Laravel, tareas como esta son fáciles. En este episodio, arreglaremos la clasificación y luego discutiremos el almacenamiento en caché **"para siempre"**.


En el archivo **routes/web.php** 

```php
Route::get('/', function () {
    $document = YamlFrontMatter::parseFile(resource_path('posts/my-fourth-post.html'));
    ddd($document->title);
});
```
En el archivo **routes/web.php** 

```php
Route::get('/', function () {
    $files = File::files(resource_path("posts/"));  
    $posts = [];
    foreach ($files as $file) {
        $document = YamlFrontMatter::parseFile($file);
        $posts [] = new Post(
            $document->title,
            $document->excerpt,
            $document->date,
            $document->body 
        );
    }

    ddd($posts);
});
```

En la clase **Posts** dentro de **app/Models/Post.php**

```php
public $title;
    public $excerpt;
    public $date;
    public $body;

    public function __construct($title, $excerpt, $date, $body){
        $this->title = $title;
        $this->excerpt = $excerpt;
        $this->date = $date;
        $this->body = $body;
    }

    public static function all(){
        return cache()->rememberForever('posts.all', function(){
            return collect(File::files(resource_path("posts/")))
            ->map(fn($file) => YamlFrontMatter::parseFile($file))
            ->map(fn($document) => new Post(
                $document->title,
                $document->excerpt,
                $document->date,
                $document->body()
            ))
            ->sortBy('date');
        })
    }
```

