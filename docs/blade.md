[<- Volver](/README.md)

# Blade: Los fundamentos absolutos

Blade es el motor de plantillas de Laravel para sus puntos de vista. Puede pensar en ello como una capa sobre PHP para hacer que la sintaxis necesaria para construir estas vistas sea lo más limpia y concisa posible. En última instancia, estas plantillas Blade se compilarán en PHP vanilla detrás de escena.

Las vistas se encuentran en:

    resources/views

Un archivo con el motor de plantilla Blade se crea de la siguiente manera:

    nombre.blade.php

Como bien se menciona para utilizar el motor de plantilla blade es importante cunado se crea una vista agregarle **.blade.php** esto para que pueda utilizar la sintaxis de Blade, si no se agrega la palabra _blade_ no funcionara si digita sintaxis de dicho motor.

Blade utiliza una sintaxis diferente a un archivo **.php** pues con blade se puede acceder facilmente a varibles, el siguiente es un ejemplo de la sintaxis de **Blade**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>POST</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    <?php
        foreach($post as $p) :
    ?>
    <article>
        {{$p}}
    </article>
    <?php endforeach; ?>
</body>
</html>
```

Con el anterior código se podrá visualizar todo la información, también las etiquetas que son utilizadas, osea en pocas palabra muestra parte del código en formato texto. 

Para que no suceda esto, solo queremos visualizar la información correspondiente sin las etiquetas html, se utiliza la siguiente sintaxis

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>POST</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    <?php
        foreach($post as $p) :
    ?>
    <article>
        {!! $p !!}
    </article>
    <?php endforeach; ?>
</body>
</html>
```

Un ejemplo más extenso utilizando el motor de plantilla blade es el siguiente:

```html
<title>
    My Blog
</title>
<link rel="stylesheet" href="/app/css/style.css">

<body>
    @foreach ($posts as $post)
    <article class ="{{ $loop->even ? 'foobar' : ''}}">
        <h1>
            <a href="/posts/{{ $post->slug}}">
            {{ $post->title}}
            </a>
        </h1>
        <div>
            {{ $post->excerpt}}
        </div>
    </article>
    @endforeach
</body>
```
También podria utilizarse la siguiente sintaxis, si se desea acceder a un atributo especifico al recorrer un **foreach**

    {{variable->atributo }}

El **@** es muy utilizado con _Blade_, el siguiente es un ejemplo de como utilizarlo:

```php
@foreach ($posts as $post)
    @if ($loop->first)
        This is the first iteration.
    @endif

    @if ($loop->last)
        This is the last iteration.
    @endif
@endforeach
```

# Algunos ajustes y consideración

Antes de pasar al siguiente capítulo, sobre bases de datos, hagamos algunos ajustes para concluir estas dos últimas secciones. Primero, eliminaremos la restricción de ruta que ya no es necesaria. Luego, consideraremos los beneficios de agregar un segundo **Post::findOrFail()** método que se anula automáticamente si no se encuentra ninguna publicación que coincida con el slug dado.

Para realizar todo lo mecionado se ocupara el suguiente código

En el archivo **routes/web.php**

```php
Route::get('post/{post}', function ($slug) {
    //Find a post by its slug and pass it to a view called "post"
    $post = Post::findOrFail($slug);

    ddd($post);

    return view('post', [
        'post' => Post::find($slug)
    ]);
});
```

Ahora en el archivo **app/Models/Post.php**

```php
public static function all(){
        return cache()->rememberForever('posts.all', function(){
            return collect(File::files(resource_path("posts/")))
            ->map(fn($file) => YamlFrontMatter::parseFile($file))
            ->map(fn($document) => new Post(
                $document->title,
                $document->excerpt,
                $document->date,
                $document->body()
            ))
            ->sortBy('date');
        })
    }

public static function find($slug){
    return static::all()->firstWhere('slug', $slug);
}

public static function findOrFail($slug){
    $post = static::find($slug);

    if(!$post){
        throw new ModelNotFoundException();
    }

    return $post;
}
```

