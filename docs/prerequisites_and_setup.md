[<- Volver](/README.md)

# Introducción a MVC

El patrón MVC va muy bien con las aplicaciones web. Esta es la base para la contrucción de aplicaciones laravel.

## Modelo

Este es quien se encarga de la logica de negocio, en esta capa es donde se manipula los datos que son almacenados en el gestor de base de datos. El modelo se comunica con el controlador.

## Vista 

Esta capa es la que se encarga de resivir los datos para ser mostrada al usuario, igualmente esta capa se comunica con el controlador. Es la que interactua con el usuario, por decirlo más simple es lo que el usurio visualiza en la aplicaión. 


## Controlador

Esta capa se sirve de enlace entre la vista y el modelo respondiendo a lo que pueda requerirse para implementar las necesidades de la aplicación. El controlador es el pendiente de las solicitudes que hace el usuario pues como dije este se comunica con el modelo y la vista, esta capa es el flujo de la aplicación.


# Compositor y configuración del entorno inicial

Para que la se pueda crear un proyecto laravel se debe tener instalado algunas herramientas, una de ellas en **Composer**, también se debe tener instalado en el computador y con la configuración adecuada PHP, Apcahe y un gestor de base de datos en este caso utilizaremos MySQL.

Se debe instalar **Composer**, con los siguientes comandos desde la terminal se lograra dicha instalación

    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    php composer-setup.php
    php -r "unlink('composer-setup.php');"

Se creara un archivo **composer.phar** luego de esto digitaremos el siguiente comando:

    php composer.phar

Seguidamente se debe mover el archivo **composer.phar** con el comando:

    mv composer.phar /usr/local/bin/composer

## Instalación de Laravel via Composer

La instalación de laravel via composer desde un terminal se realiza con el siguiente comando: 

    composer create-project laravel/laravel _nombre_app_





