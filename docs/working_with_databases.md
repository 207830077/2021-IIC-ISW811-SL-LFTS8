[<- Volver](/README.md)

# Archivos de entorno y conexiones de bases de datos

Cada aplicación requerirá una cierta cantidad de configuración específica del entorno. Ejemplos de esto pueden ser el nombre de la base de datos a la que se está conectando, o qué host de correo y puerto utiliza su aplicación, o incluso claves especiales y tokens secretos que le proporcionan las API de terceros. Puede almacenar una configuración como esta dentro de su **.env** archivo, que se encuentra en la raíz de su proyecto. En este episodio, discutiremos los **aspectos** esenciales de los archivos de entorno y luego pasaremos a la conexión a una base de datos MySQL (usando **TablePlus** ).

Lo primero que se debe hacer es comprobar la configuración del archivo **.env**
  
    DB_CONNECTION = mysql
    DB_HOST = 127.0.0.1
    DB_PORT = 3306
    DB_DATABASE =nombre_database
    DB_USERNAME = user
    DB_PASSWORD  = password

Para crear una base de datos en **mysql** se digita el comando

    create database nombre_database;

Seguidamente se crea una migración artisan, esto fuera del entorno de _mysql_, con el comando: 

    php artisan migrate

Se requiere instalar un programa para el manejo de la base de datos, el siguiente es el link de referencia a la descarga del programa

* [TablePlus](https://tableplus.com/)

Una vez instaldo **TablePlus** se realiza la conección a **mysql** con dicho programa, para esto se agrega las credenciales correspondientes, las que se configuraron en el archivo **.env**

# Migraciones: los conceptos básicos absolutos

Ahora que nos hemos conectado correctamente a MySQL, pasemos nuestra atención a esas misteriosas clases de migración. Piense en una migración como un modelo para una tabla de base de datos.

Lo siguiente es una porción de código sobre la migración

```php
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('is_admin')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
```

Las migraciones se encuentran en el archivo **database/migrations**

Si se desea retorceder una migración, se podria realizar con el siguiente comando:

    php artisan migrate:rollback

Además se puede visualizar los diferentes comandos que posee **artisan**, no solo _migrate_ existe una gran cantidad de comandos que puede ser muy utiles, con el siguientes comando se puede visualizar todos lo comandos en general

    php artisan

# Eloquent y el patrón de registro activo

Pasemos ahora a **Eloquent**, que es la implementación de Active Record de Laravel. Eloquent nos permite mapear un registro de tabla de base de datos a un objeto Eloquent correspondiente. En este episodio, aprenderá la API inicial, que le resultará bastante familiar si siguió el capítulo anterior.

Desde la consola haremos uso de **tinker**, esto con el siguiente comando:

    php artisan tinker

Una vez hecho lo anterior enviaremos una porción de código, es el siguiente:

    >>> $user = new App\Models\User;

Tendremos como respuesta

    => App\Models\User {#3379}

Ahora haremos un **insert** a la base de datos, en la tabla **user** utilizando **tinker**

    >>> $user = new User;
    >>> $user->name = 'Geiner';
    => "Geiner"
    >>> $user->email = 'gchavarriag25@gmail.com';
    => "gchavarriag25@gmail.com"
    >>> $user->password = bcrypt('!password');
    => "$2y$10$qFftacmF0NQi53GXKaAlDeiBNWcml4mKYIw7aqRSL1C1As4TebWiS"
    >>> $user->save();
    => true

Una vez **insertado** el usuario desde tinker en la consola se puede obtener el usuario indicandole el **id**, esto con el siguiente comando:

    >>> User::find(1);

Retornara una respuesta como la siguiente:

    => App\Models\User {#4106
     id: 1,
     name: "Geiner",
     email: "gchavarriag25@gmail.com",
     email_verified_at: null,
     #password: "$2y$10$qFftacmF0NQi53GXKaAlDeiBNWcml4mKYIw7aqRSL1C1As4TebWiS",
     #remember_token: null,
     created_at: "2021-06-03 03:31:37",
     updated_at: "2021-06-03 03:31:37",
   }

Si se desea retornar todos los usuarios, seria con el siguiente comando:

    >>> User::all();

# Hacer un modelo de publicación y migración

Ahora que está un poco más familiarizado con las clases de migración y los modelos Eloquent, apliquemos este aprendizaje a nuestro proyecto de blog. Eliminaremos la implementación antigua basada en archivos del capítulo anterior y la reemplazaremos con un nuevo **Post** modelo Eloquent. También prepararemos una migración para construir la **posts** mesa.

Para crear una nueva migración llamada **create_posts_table** se ejecuta el siguiente comando:

    php artisan make:migration create_posts_table

Seguidamente se crea una migración la cual se puede visualizar en **database/migrations**, contendra un código similar al siguiente:

```php
/**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
```

Ahora agregaremos algunas columna a la tabla **posts**

```php
Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('excerpt');
            $table->text('body');
            $table->timestamps();
            $table->timestamp('published_at')->nullable();
        });
```

Y se ejecuta el debido comando para migrar

    php artisan migrate

Posterior se creara un modelo correspondiente, esto con el siguiente comando:

    php artisan make:model Post

Ahora nuevamente se hara uso de **tinker** para eso con el comando:

    php artisan tinker

Se va a crear un nuevos **post** y sera insertado en la base de datos

    >>> $post = new App\Models\Post
    => App\Models\Post {#3378}
    >>> $post->title = 'My First Post';
    => "My First Post"
    >>> $post->excerpt = 'Lorem ipsum dolar sit amet.';
    => "Lorem ipsum dolar sit amet."
    >>> $post->body = 'Lorem ipsum dolor sit amet. Sapiente deleniti eligendi, reprehenderit non dolorum unde ab adipisci so
    luta cum, optio consequuntur iure eveniet cupiditate laudantium consectetur debitis rem suscipit impedit?';
    => "Lorem ipsum dolor sit amet. Sapiente deleniti eligendi, reprehenderit non dolorum unde ab adipisci soluta cum, optio consequuntur iure eveniet cupiditate laudantium consectetur debitis rem suscipit impedit?"
    >>> $post->save();
    => true

Para mostrar los datos insertados en la base de datos se procede con el siguiente código

En el archivo **routes/web.php**

```php
Route::get('/', function () {
    return view('posts', [
        'posts' => Post::all()
    ]);
});

Route::get('posts/{post}', function ($id) {
    //Find a post by its slug and pass it to a view called "post"

    return view('post', [
        'post' => Post::findOrFail($id)
    ]);
});
```

Seguidamente en el archivo **resources/views/posts.blade.php** 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>POST</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    
    @foreach($posts as $post) 
    <article>
            <h1>
                <a href="/posts/{{ $post->id }}">
                    {{ $post->title }}
                </a>
            </h1>
            <div>
                {{ $post->excerpt }}
            </div>
    </article>
    @endforeach

</body>
</html>
```

Y en el archivo **resources/views/post.blade.php**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Blog</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
<article>
    <h1>{{ $post->title }}</h1>
    <div>
        {!! $post->body !!}
    </div>
</article>
<br>
<a href="/">Go Back</a>
</body>
</html>
```

# Actualizaciones elocuentes y escape HTML

En esta lección, discutiremos brevemente cómo actualizar los registros de la base de datos usando Eloquent. Luego, revisaremos un ejemplo de por qué escapar de la entrada proporcionada por el usuario es esencial para la seguridad de su aplicación.

Para esto, lo primero que se debe hacer es ingresar a la terminal e iniciar **tinker**, digitar el siguiente comando:

    php artisan tinker

Luego de esto procederemos a insertar, actualizar y demás registros a la base de datos usando **Eloquent**

    >>> $post = new App\Models\Post::find(2);
    => App\Models\Post {#4060}
    >>> $post->title = 'My First Post';
    id: 1,
    title: "My First Post"
    excerpt: "Lorem ipsum dolar sit amet."
    body: "Lorem ipsum dolor sit amet. Sapiente deleniti eligendi, reprehenderit non dolorum unde ab adipisci soluta cum, optio consequuntur iure eveniet cupiditate laudantium consectetur debitis rem suscipit impedit?"

Se puede agregar etiquetas **HTML** a los atributos del **Post**, el siguiente es un ejemplo de como hacerlo

    >>> $post->body = '<p>' .$post->body . '</p>';
    >>> $post->save();
    => true

Esto lo que hara es agregarle una etiqueta HTML de tipo **parrafo** al body del post.


# Tres formas de mitigar las vulnerabilidades de las asignaciones masivas

En esta lección, analizaremos todo lo que necesita saber sobre las vulnerabilidades de las asignaciones masivas. Como verá, Laravel proporciona un par de formas de especificar qué atributos pueden o no asignarse en masa. Sin embargo, hay una tercera opción al final de este video que es igualmente válida.

Primero se agregara un tercer post, como el siguiente:

    $ php artisan tinker
    Psy Shell v0.10.8 (PHP 8.0.6 — cli) by Justin Hileman
    >>> use App\Models\Post;
    >>> $post = new Post;
    => App\Models\Post {#3377}
    >>> $post->title = 'My Third Post';
    => "My Third Post"
    >>> $post->excerpt = 'excerpt of post';
    => "excerpt of post"
    >>> $post->body = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Op
    tio maiores doloremque voluptatum veniam quaerat error dolorem officiis magnam n
    atus voluptatem aliquid, unde delectus necessitatibus. Dolor excepturi asperiore
    s rem ea nostrum.';
    => "\x16\x16Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio maior
    es doloremque voluptatum veniam quaerat error dolorem officiis magnam natus volu
    ptatem aliquid, unde delectus necessitatibus. Dolor excepturi asperiores rem ea
    nostrum."
    >>> $post->save();
    => true

Si se desea consultar todos los registro, se hace con el comando **Post::all();** a continuación un ejemplo:

    >>> Post::all();
    => Illuminate\Database\Eloquent\Collection {#3698
        all: [
        App\Models\Post {#4066
            id: 1,
            title: "My First Post",
            excerpt: "Lorem ipsum dolar sit amet.",
            body: "Lorem ipsum dolor sit amet. Sapiente deleniti eligendi, reprehen
    derit non dolorum unde ab adipisci soluta cum, optio consequuntur iure eveniet c
    upiditate laudantium consectetur debitis rem suscipit impedit?",
            created_at: "2021-06-03 04:16:24",
            updated_at: "2021-06-03 04:16:24",
            published_at: null,
        },
        App\Models\Post {#4313
            id: 2,
            title: "My Second Post",
            excerpt: "Lorem ipsum dolar sit amet.",
            body: "Lorem ipsum dolor sit amet. Sapiente deleniti eligendi, reprehen
    derit non dolorum unde ab adipisci soluta cum, optio consequuntur iure eveniet c
    upiditate laudantium consectetur debitis rem suscipit impedit?",
            created_at: "2021-06-03 18:21:23",
            updated_at: "2021-06-03 18:21:38",
            published_at: null,
        },
        App\Models\Post {#4314
            id: 3,
            title: "My Third Post",
            excerpt: "excerpt of post",
            body: "\x16\x16Lorem ipsum dolor sit amet consectetur adipisicing elit.
    Optio maiores doloremque voluptatum veniam quaerat error dolorem officiis magna
    m natus voluptatem aliquid, unde delectus necessitatibus. Dolor excepturi asperi
    ores rem ea nostrum.",
            created_at: "2021-06-04 00:50:13",
            updated_at: "2021-06-04 00:50:13",
            published_at: null,
        },
        ],
    }

Se procede a crear un cuarto con una sintaxis diferentes, lo primero que se debe hacer es ir al archivo**app/Models/Post.php** agregar los atributos que se desean agregar al nuevo post.

```php
protected $fillable = ['title', 'excerpt', 'body'];
```

Una vez realizado lo anterior se procede a realizar la creación del nuevos post utilizando **tinker**, el siguiente código realiza la creación del nuevo post.
    
    php artisan tinker

    >>> Post::create(['title' => 'My Fourth Post', 'excerpt' => 'excerpt of post', 'body'
    => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.  Optio maiores doloremq
    ue voluptatum veniam quaerat error dolorem officiis mm natus voluptatem aliquid, unde
    delectus necessitatibus. Dolor excepturi asperiores rem ea nostrum.']);
    [!] Aliasing 'Post' to 'App\Models\Post' for this Tinker session.



# Enlace de modelo de ruta

La función de enlace del modelo de ruta de Laravel nos permite vincular un comodín de ruta a una instancia de modelo Eloquent.

En el archivo **routes/wen.php** se modifica una porción del código el cual quedaria de la siguiente manera

```php
Route::get('posts/{post}', function (Post $post) {
    return view('post', [
        'post' => $post
    ]);
```

Ahora en el archivo de migración **create_posts_table.php** se agrega una nueva columna llamada **slug**

```php
Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->unique();
            $table->string('title');
            $table->text('excerpt');
            $table->text('body');
            $table->timestamps();   
            $table->timestamp('published_at')->nullable();
        });
```

Se vuelve a crear los nuevos post, pero ahora se le agregó un **slug** seguidamente se modifica el archivo **routes/web.php**

```php
Route::get('posts/{post:slug}', function (Post $post) { //Post::where('slug', $post)->firstorFail()
    return view('post', [
        'post' => $post
    ]);
});
```

Después de esto se modifica el archivo **resources/views/posts.blade.php** esto para que a la hora de hacer un GET no muestre el id del post sino el slug, esto se hace de la siguiente manera

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>POST</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    
    @foreach($posts as $post) 
    <article>
            <h1>
                <a href="/posts/{{ $post->slug }}">
                    {{ $post->title }}
                </a>
            </h1>
            <div>
                {{ $post->excerpt }}
            </div>
    </article>
    @endforeach

</body>
</html>
```

# Tu primera relación elocuente

Nuestro próximo trabajo es descubrir cómo asignar una categoría a cada publicación. Para permitir esto, necesitaremos crear un nuevo modelo de Eloquent y una migración para representar un archivo **Category**.

Se crea una nueva migracción esta vez con sea de **categoria

    php artisan make:migration create_categories_table

Una vez creada la migración se procede a crear el modelo para las categorias, esto con el siguiente comando:

    php artisan make:model Category -m

Seguidamente se crea el correspondiente **Schema** para las categorias para esto se modifica el archivo **database/migrations/create_categories_table.php**

```php
Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });
```

En el archivo  **database/migrations/create_posts_table.php** se agrega una nueva columna al Schema que sera una Foreing de la tabla category

```php
$table->foreignId('category_id');
```
Seguidamente se insertan algunas categorias en la base de datos, posterior se procede a insertar nuevos post pero ahora se le agrega el **id** de la categoria en la comuna **category_id** esto para hacer una referencia a que categoria es.

Se procede a crear una porción de código el cual tendra como fin poder mostrar los datos de la categoria que tiene como referencia el post que fue creado. El siguiente es el código que hara dicha función, esto en el archivo **App/Models/Post.php**:

```php
public function category(){
        return $this->belongsTo(Category::class);
    }
```

Ahora en el archivo **resources/views/posts.blade.php** se puede mostrar los datos de la categoria a la cual esta referenciada en el post

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>POST</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>

    @foreach($posts as $post)
    <article>
            <h1>
                <a href="/posts/{{ $post->slug }}">
                    {{ $post->title }}
                </a>
            </h1>
            <!-- Aquí se muestra el nombre de la categoria del Post <!-->
            <a href="#">{{ $post->category->name }}</a>

            <div>
                <p>{{ $post->excerpt }}</p>
            </div>
    </article>
    @endforeach

</body>
</html>
```

# Mostrar todas las publicaciones asociadas a una categoría

Ahora que tenemos el concepto de una **Categoria** en nuestra aplicación, creemos una nueva ruta que obtenga y cargue todas las publicaciones asociadas con la categoría dada.

Primero se crea una nueva ruta esto en el archivo **routes/web.php**

```php
Route::get('categories/{category}', function (Category $category) {
    return view('posts', [
        'posts' => $category->posts
    ]);
});
```

Seguidamente en el archivo **app/Models/Category.php** se agrega el siguiente código el cual es una función para mostrar los datos de un post que tengan alguna categoria en especifico asociada:

```php
public function posts(){
        return $this->hasMany(Post::class);
    }
```

Ahora en las vista de los posts se agrega una etiqueta **a** para navegar por el id de la categoria, el siguiente es como debe quedar el código de la vista en el archivo **resources/views/posts.blade.php** y **resources/views/post.blade.php**

* **posts.blade.php**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>POST</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>

    @foreach($posts as $post)
    <article>
            <h1>
                <a href="/posts/{{ $post->slug }}">
                    {!! $post->title !!}
                </a>
            </h1>
            <a href="/categories/{{ $post->category->id }} ">{{ $post->category->name }}</a>
            <div>
                <p>{!! $post->excerpt !!}</p>
            </div>
    </article>
    @endforeach

</body>
</html>
```

* **post.blade.php**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Blog</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
<article>
    <h1>{!! $post->title !!}</h1>

    <p>
        <a href="/categories/{{ $post->category->id }} ">{{ $post->category->name }}</a>
    </p>

    <div>
        <p>{!! $post->body !!}</p>
    </div>
</article>
<br>
<a href="/">Go Back</a>
</body>
</html>
```

# Mecanismo de relojería y el problema N + 1

Introdujimos un problema de rendimiento sutil en el último episodio que se conoce como el problema N + 1. Debido a las relaciones de carga diferida de Laravel, esto significa que potencialmente puede caer en una trampa en la que se ejecuta una consulta SQL adicional para cada elemento dentro de un bucle. Cincuenta elementos ... cincuenta consultas SQL. En este episodio, le mostraré cómo depurar estas consultas, tanto manualmente como con la excelente **extensión Clockwork** , y luego resolveremos el problema cargando ansiosamente cualquier relación a la que hagamos referencia.

Link para instalar **Clockwork** via composer

* [Clockwork](https://github.com/itsgoingd/clockwork)

También está disponible una extensión de herramientas de desarrollo del navegador para Chrome y Firefox:

* [Chrome](https://chrome.google.com/webstore/detail/clockwork/dmggabnehkmmfmdffgajcflpdjlnoemp)
* [Firefox](https://addons.mozilla.org/en-US/firefox/addon/clockwork-dev-tools/)

Se modifica el código en el archivo **routes/web.php** 

```php
Route::get('/', function () {
    return view('posts', [
        'posts' => Post::with('category')->get()
    ]);
});
```

# La inicialización de bases de datos ahorra tiempo

En esta lección, asociaremos una publicación de blog con un autor o usuario en particular. Sin embargo, en el proceso de agregar esto, nos volveremos a encontrar con el problema de tener que volver a llenar manualmente nuestra base de datos. Este podría ser un buen momento para tomarse unos minutos para revisar la siembra de la base de datos. Como verá, un poco de trabajo por adelantado le ahorrará mucho tiempo a largo plazo.

Se modifica el archivo **app/Models/User.php**

```php
public function posts() //$user->posts
    {
        return $this->HasMany(Post::class);
    }
```

Se modifica el archivo **app/Models/Post.php**

```php
public function user()
    {
        return $this->belongsTo(User::class);
    }
```

Se modifica el archivo **database/seeders/DatabaseSeeder.php**

```php
User::truncate();
Post::truncate();
Category::truncate();

$user = \App\Models\User::factory()->create();

$personal = Category::create([
        'name' => 'Personal',
        'slug' => 'personal'
]);

$family = Category::create([
    'name' => 'Family',
    'slug' => 'family'
]);

$work = Category::create([
    'name' => 'Work',
    'slug' => 'work'
]);

Post::create([
    'user_id' => $user->id,
    'category_id' => $personal->id,
    'title' => 'My Personal Post',
    'slug' => 'my-personal-post',
    'excerpt' => 'Lorem ipsum dolar sit amet.',
    'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis perferendis, modi fugiat consequatur est doloremque eius rem debitis corporis saepe eaque dolores ab error voluptate laudantium adipisci magni possimus mollitia?'
]);

Post::create([
    'user_id' => $user->id,
    'category_id' => $family->id,
    'title' => 'My Family Post',
    'slug' => 'my-family-post',
    'excerpt' => 'Lorem ipsum dolar sit amet.',
    'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis perferendis, modi fugiat consequatur est doloremque eius rem debitis corporis saepe eaque dolores ab error voluptate laudantium adipisci magni possimus mollitia?'
]);

Post::create([
    'user_id' => $user->id,
    'category_id' => $work->id,
    'title' => 'My Work Post',
    'slug' => 'my-work-post',
    'excerpt' => 'Lorem ipsum dolar sit amet.',
    'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis perferendis, modi fugiat consequatur est doloremque eius rem debitis corporis saepe eaque dolores ab error voluptate laudantium adipisci magni possimus mollitia?'
]);
```
Se ejecuta el siguiente comando en la terminal para completar el **Seeding**

    php artisan db:seed

# Impulso de Turbo con Factories

Ahora que comprende los conceptos básicos de las sembradoras de bases de datos, integremos fábricas de modelos para generar sin problemas cualquier número de registros de bases de datos.

Para crear multiples usuarios en liniea de comando usando tinker, el siguiente comando realiza esas multiples inserciones a la base de datos, se envia por parametro la cantidad de registros que se desea.

    php artisan tinker

    >>>App\Models\User::factory(50)->create();  //aquí se crearan 50 registros de usuarios

Para crear un **factorie** de alguna clase, en este caso sera la clase **Post.php** se utiliza el siguiente comando:

    php artisan make:factory PostFactory

Luego de ejecutar el comando anterior se crea una Clase llamada **PostFactory** en **database/factories**

```php
<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}

```

Para crear un Modelo, un Factory y una Migración con el nombre **Comment**, sse utiliza el comando:

    php artisan make:model Comment -mf

    Model created successfully.
    Factory created successfully.
    Created Migration: 2021_06_05_010132_create_comments_table

En el archivo **database/factories/PostFactory.php** se modifica la función **definition()** quedaria de la siguiente manera:

```php
public function definition()
    {
        return [
            'user_id' => User::factory(),
            'category_id' => Category::factory(),
            'title' => $this->faker->sentence,
            'excerpt' => $this->faker->sentence,
            'body' => $this->faker->paragraph
        ];
    }
```

Luego de esto se hace un **migrate sfresh** con el comando:

    php artisan migrate:fresh

Posteriormente se crea un factory para las categorias, esto con el comando:

    php artisan make:factory CategoryFactory

Se modifica el archivo **database/factories/CategoryFactory.php**

```php
public function definition()
    {
        return [
            'name' => $this->faker->word,
            'slug' => $this->faker->slug,
        ];
    }
```

Además se mofifica el archivo **database/factories/PostFactory.php** se le agrega el atributo **slug** a la funcion **definition()**

```php
'slug' => $this->faker->slug
```

Quedaria de la siguiente manera la funcion completa:

```php
public function definition()
    {
        return [
            'user_id' => User::factory(),
            'category_id' => Category::factory(),
            'title' => $this->faker->sentence,
            'slug' => $this->faker->slug,
            'excerpt' => $this->faker->sentence,
            'body' => $this->faker->paragraph
        ];
    }
```
Después de esto se procede a crear un **Post factory** esto crea un usuario, una categoria y el post

    php artisan tinker

    >>>App\Models\Post::factory()->create();

Ahora para crear los factory por **seeders** se modifica el archivo **databases/seeders/DatabaseSeeder.php**

```php
public function run()
    {
        Post::factory(5)->create();
    }
```

Luego de esto se ejecuta el siguiente comando para crear los post atravez de **seed**

    php artisan migrate:fresh --seed

# Ver todas las publicaciones de un autor

Ahora que podemos asociar una publicación de blog con un autor, el siguiente paso obvio es crear una nueva ruta que muestre todas las publicaciones de blog escritas por un autor en particular.

Lo primero que se debe hacer es modificar el archivo **app/Models/Post.php** la cambia la función **user** por **author** y se le agregar como parametro **user_id**

```php
public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
```

Seguidamente se modifica el archivo **database/factories/UserFactory.php** la función llamada **definition** se le agrega un nuevo atributo el cual sera _username_

```php
public function definition()
    {
        return [
            'username' => $this->faker->unique()->userName,
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];
    }
```

Posterior a esto se modifica el archivo **database/migrations/crrate_user_table.php** se agrega el atributo _usernname_

```php
Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->unique();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
```

Se modifican las vistas de la siguiente manera:

* posts.blade.php

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>POST</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>

    @foreach($posts as $post)
    <article>
            <h1>
                <a href="/posts/{{ $post->slug }}">
                    {!! $post->title !!}
                </a>
            </h1>
            <p>
            By <a href="/authors/{{ $post->author->username }}">{{ $post->author->name }}</a> in <a href="/categories/{{ $post->category->slug }} ">{{ $post->category->name }}</a>
            </p>
            <div>
                <p>{!! $post->excerpt !!}</p>
            </div>
    </article>
    @endforeach

</body>
</html>
```

* post.blade.php

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Blog</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
<article>
    <h1>{!! $post->title !!}</h1>

    <p>
        By <a href="/authors/{{ $post->author->username }}">{{ $post->author->name }}</a> in <a href="/categories/{{ $post->category->slug }} ">{{ $post->category->name }}</a>
    </p>

    <div>
        <p>{!! $post->body !!}</p>
    </div>
</article>
<br>
<a href="/">Go Back</a>
</body>
</html>
```

Después de esto se procede a crear la ruta, se agrega el siguiente código al archivo **routes/web.php**

```php
Route::get('authors/{author:username}', function (User $author) {
    return view('posts', [
        'posts' => $author->posts
    ]);
});
```

Y por ultimo se ejecuta el siguiente comando en la terminal para hacer un fresh al seed

    php artisan migrate:fresh --seed

Con esto terminaria la seccion de trabajo con base de datos.




