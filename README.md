# Curso Laravel From Scrath 8

## Link referente al curso:

* [Laravel From Scrath 8](https://laracasts.com/series/laravel-8-from-scratch)

# Sections 

## Section 1

* [Prerequisites and Setup](./docs/prerequisites_and_setup.md)

## Section 2

* [The Basic](./docs/the_basic.md)

## Section 3

* [Blade](./docs/blade.md) 

## Section 4

* [Working With Databases](./docs/working_with_databases.md)

## Section 5

* [Integrate the Desing](./docs/integrate_desing.md)

## Section 6

* [Search](./docs/search.md)

## Section 7

* [Filtering](./docs/filtering.md)

## Section 8

* [Pagination](./docs/pagination.md)

# Evidence of completion of the course

![img](./docs/img/evidencia.png)
