<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory; //Post::factory()

    //protected $fillable = ['title', 'excerpt', 'body', 'id'];
    protected $fillable = ['title', 'excerpt', 'body'];

    protected $with = ['category', 'author'];
    //protected $guarded = ['id'];
    //protected $guarded = [];

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? false, function ($query, $search){
            $query
                ->where('title', 'like', '%' . $search . '%')
                ->orWhere('body', 'like', '%' . $search . '%');
        });

        $query->when($filters['category'] ?? false, function ($query, $category){
            $query
                ->whereExists(
                    $query->from('categories')
                     ->where('categories.id', 'posts.category_id')
                     ->where('categories.slug', $category)
                );
        });

        $query->when($filters['author'] ?? false, function ($query, $author){
            $query
                ->whereExists(
                    $query->from('authors')
                     ->where('authors.username', $author)
                );
        });
    }

    public function getRouteKeyName(){
        return 'slug';
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    /**
     * Get the user that owns the Post
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
